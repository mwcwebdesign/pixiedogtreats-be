from flask import Flask
from flask import jsonify
from flask import request
from bson.json_util import dumps
import pymongo

app = Flask(__name__)

myclient = pymongo.MongoClient("mongodb://192.168.16.20:27017/")
mydb = myclient["tpixietreats"]

@app.route('/')
def root():
	return "Welcome to the backend API"


@app.route('/help')
@app.route('/help/')
def help():
	help = {
		"/authenticateUser":"Used to Authenticate a user. Requires usernam and password",
		"/logOut":"Clears the token and logs user out",
		"/find":"Used to find a user"
	}
	return jsonify(help)


@app.route('/authenticateUser', methods=["POST"])
@app.route('/authenticateUser/', methods=["POST"])
def authenticateUser():
	if request.method == "POST":
		myusers = mydb['userData']
		
		content = request.get_json()
		user = content.get('email')
		passw = content.get('password')
		#user = request.form["email"]
		#passw = request.form["password"]

		users = dumps(myusers.find_one({'email':user, 'password':passw}))
		return jsonify(users)
	else:
		return "Bo"


@app.route('/logOut')
@app.route('/logOut/')
def logOut():
	return


@app.route('/addItemCart', methods=['POST'])
@app.route('/addItemCart/', methods=['POST'])
def addItem():
	if request.method == "POST":
		usercol = mydb['userData']

		itemNo = request.form['itemNo']
		user = request.form['email']

		mydoc = usercol.find_one({'email':user})
		cart = mydoc['cart']
		cart.append(itemNo)

		query = {'email': user}
		update = {"$set": { "cart" : cart}}

		
		usercol.update_one(query, update)

		return True


@app.route('/removeItemCart', methods=['POST'])
@app.route('/removeItemCart/', methods=['POST'])
def removeItem():
	if request.method == "POST":
		usercol = mydb['userData']

		user = request.form['email']
		itemNo = request.form['itemNo']

		mydoc = usercol.find_one({'email':user})
		cart = mydoc['cart']
		cart.remove(itemNo)

		query = {'email': user}
		update = {"$set": { "cart" : cart}}
		
		usercol.update_one(query, update)

		return True


@app.route('/getCart', methods=['POST'])
@app.route('/getCart/', methods=['POST'])
def getCart():
	if request.method == "POST":
		usercol = mydb['userData']

		user = request.form['email']

		mydoc = usercol.find_one({'email':user})
		cart = mydoc['cart']

		return cart


@app.route('/getProducts', methods=['GET'])
@app.route('/getProducts/', methods=['GET'])
def getProducts():
	info =  '[{"_id": {"$oid": "5bdda5a0f12c333519608ec4"}, "itemNo": "pt0001-lg", "price": 2.99, "name": "Peanut Butter Bar Large", "desc": "Delicious Peanut Butter Bars for Large Dogs", "image": "peanut_butter_bars_lg.JPG"}, {"_id": {"$oid": "5bdda5a0f12c333519608ec5"}, "itemNo": "pt0001-md", "price": 1.99, "name": "Peanut Butter Bar Medium", "desc": "Delicious Peanut Butter Bars for Medium Dogs", "image": "peanut_butter_bars_lg.JPG"}, {"_id": {"$oid": "5bdda5a0f12c333519608ec6"}, "itemNo": "pt0001-sm", "price": 1.99, "name": "Peanut Butter Bar Small", "desc": "Delicious Peanut Butter Bars for Small Dogs", "image": "peanut_butter_bars_lg.JPG"}, {"_id": {"$oid": "5bdda5a0f12c333519608ec7"}, "itemNo": "pt0002", "price": 5.99, "name": "Duck Flavored Training Treats", "desc": "Tiny duck flavored training treats easy for your dog or puppy to eat", "image": "duck-training-treats.jpg"}, {"_id": {"$oid": "5bdda5a0f12c333519608ec8"}, "itemNo": "pt0003", "price": 5.99, "name": "Chicken Flavored Training Treats", "desc": "Tiny chicken flavored training treats easy for your dog or puppy to eat", "image": "chicken-training-treats.jpg"}, {"_id": {"$oid": "5bdda5a0f12c333519608ec9"}, "itemNo": "pt0004", "price": 5.99, "name": "Bacon Flavored Training Treats", "desc": "Tiny bacon flavored training treats easy for your dog or puppy to eat", "image": "bacon-training-treats.jpg"}, {"_id": {"$oid": "5bdda5a0f12c333519608eca"}, "itemNo": "pt0005", "price": 7.99, "name": "Organic Duck Flavored Treats", "desc": "Completly Organic Duck flavored treats. Great for your dog or puppys digestion", "image": "organic-duck-training-treats.jpg"}, {"_id": {"$oid": "5bdda5a0f12c333519608ecb"}, "itemNo": "pt0006", "price": 7.99, "name": "Organic Chicken Flavored Treats", "desc": "Completly Organic Chicken flavored treats. Great for your dog or puppys digestion", "image": "organic-chicken-training-treats.jpg"}, {"_id": {"$oid": "5bdda5a0f12c333519608ecc"}, "itemNo": "pt0007", "price": 7.99, "name": "Organic Bacon Flavored Treats", "desc": "Completly Organic Bacon flavored treats. Great for your dog or puppys digestion", "image": "organic-bacon-training-treats.jpg"}, {"_id": {"$oid": "5bdda5a2f12c333519608ecd"}, "itemNo": "pt0008", "price": 7.99, "name": "Organic Lamb Flavored Treats", "desc": "Completly Organic Lamb flavored treats. Great for your dog or puppys digestion", "image": "organic-lamb-training-treats.jpg"}]'
	
	#if request.method == 'GET':
	#	curitems = mydb['products']
	#	items = dumps(curitems.find())
	return info


@app.route('/getItem/<itemNo>', methods=['GET'])
def getItems(itemNo = None):
	if request.method == 'GET' and itemNo is not None:
		curitems = mydb['products']
		item = dumps(curitems.find({'itemNo':itemNo}))
		return item
	else:
		return False



if __name__ == '__main__':
	app.run(debug=True)
