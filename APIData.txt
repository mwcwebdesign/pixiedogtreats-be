API Data

User Authentication
    authenticateUser
    logOut
    
Shopping Cart
    addItem
    removeItem
    getItems
    
User Information
    getUserData
    updateUserData



MongoDB Collections

    userData
        username
        password
        orders
            order#
        cart
            item#
